package fr.istic.hue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;

import org.glassfish.jersey.client.ClientResponse;


public class Pairing {


	@XmlRootElement
	public static class PairingRequest{
		public String devicetype;
	}
	
	
	@XmlRootElement
	public static class HuePairingResponse{
		public Error error;
		public Success success;
		
		@XmlRootElement
		public static class Error{
			public String type;
			public String address;
			public String description;
			public String toString() {
				return "type="+type+ " address="+address+ " description= "+ description;
			}
		}
		
		@XmlRootElement
		public static class Success{
			public String username;
			public String toString() {
				return "username="+username;
			}
		}
		
		@Override
		public String toString() {
			return "error=["+error + "] success = ["+ success +"]";
		}
	}
	
	public static void main(String[] args) {
		
		
		Client client = ClientBuilder.newClient();
		
		PairingRequest pairingRequest = new PairingRequest();
		pairingRequest.devicetype="my_hue_app#iphone peter";
		
		HuePairingResponse[] responses = client.target(Configuration.BRIDGE_URL).path("api")
				.request(MediaType.APPLICATION_JSON_TYPE)
				.post(Entity.entity(pairingRequest, MediaType.APPLICATION_JSON), HuePairingResponse[].class);
		for (HuePairingResponse response : responses) {
			System.out.println(response);
			if (response.success != null){
				System.out.println("The username to use is "+ response.success.username);
			}else{
				System.out.println("There was an error =  "+ response.error.description + " when calling "+ response.error.address);
			}
		}
	}
	
	
}
