package hue.tp;



import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;


public class Light {

	private static int lightID;


	public Light(int lightID){
		this.lightID = lightID;
	}

	public State getState(){
		LightDescription description = getLightDescription(lightID);
		if(description.state != null){
			return description.state;
		}else{
			throw new IllegalStateException("Could not retrieve state");
		}
	}

	public boolean isOn(){
		State state = getState();
		if(state != null){
			return state.on;
		}else{
			throw new IllegalStateException("Could not retrieve state");
		}
	}

	public int getHue(){
		State state = getState();
		return state.hue;
	}


	public void setHue(int hue){
		//retrieve the current state :
		LightDescription description = getLightDescription(lightID);
		State newState = description.state;
		//toggle the light :
		newState.hue =  hue;
		//send the new state
		setState(lightID, newState);
	}


	public int getSaturation(){
		State state = getState();
		return state.sat;
	}


	public void setSaturation(int sat){
		//retrieve the current state :
		LightDescription description = getLightDescription(lightID);
		State newState = description.state;
		//toggle the light :
		newState.sat =  sat;
		//send the new state
		setState(lightID, newState);
	}

	public int getBrightness(){
		State state = getState();
		return state.bri;
	}


	public void setBrightness(int bri){
		//retrieve the current state :
		LightDescription description = getLightDescription(lightID);
		State newState = description.state;
		//toggle the light :
		newState.bri =  bri;
		//send the new state
		setState(lightID, newState);
	}



	public void toggle(){
		//retrieve the current state :
		LightDescription description = getLightDescription(lightID);
		State newState = description.state;
		//toggle the light :
		newState.on = ! description.state.on;
		//send the new state
		setState(lightID, newState);
	}



	@XmlRootElement
	public static class State{
		public boolean on;
		public int sat;
		public int bri;
		public int hue;

		@Override
		public String toString() {
			return "on="+on + " sat = "+sat + " bri= "+ bri + " hue="+ hue;
		}

	}


	public static void main(String[] args) {
		Light light3 = new Light(1);
		light3.toggle();
		System.out.println(light3.isOn());
		Random r = new Random();

		for (int i = 0; i < 1000; i++){
			light3.setHue(r.nextInt(10000));
			System.out.println("on = "+ light3.isOn());
			System.out.println("hue ="+ light3.getHue());
			light3.setSaturation(r.nextInt(255));
			System.out.println("sat ="+ light3.getSaturation());
			light3.setBrightness(r.nextInt(255));
			System.out.println("bri= "+ light3.getBrightness());
			System.out.println("--------["+i+"]");

		}
	}

	@XmlRootElement
	public static class LightDescription{
		public State state;

		@Override
		public String toString() {
			return state.toString();
		}
	}

	@XmlRootElement
	public static class HueLightResponse{

	}




	public static String setState(int number, State newState){
		Client client = ClientBuilder.newClient();
		String response =  client.target(Configuration.BRIDGE_URL).path("api").path(Configuration.KEY).path("lights")
				.path(String.valueOf(number)).path("state")
				.request(MediaType.APPLICATION_JSON_TYPE)
				.put(Entity.entity(newState, MediaType.APPLICATION_JSON), String.class);
		System.out.println("set state response = "+ response);
		return response;

	}

	public static LightDescription getLightDescription(int number){
		Client client = ClientBuilder.newClient();
		return 	client.target(Configuration.BRIDGE_URL).path("api").path(Configuration.KEY).path("lights").path(String.valueOf(number))
				.request(MediaType.APPLICATION_JSON_TYPE)
				.get(LightDescription.class);

	}

}
